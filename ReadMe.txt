HELLo and welcome to Hearts of Demons - ARCHVILE. This document serves as a quick start guide to the mod.

First up the HUD, health and armor are on the left, Mana and Magic Level are on the right. Magic Level is gained when you get a soul gem and 
effects your mana regeneration.

You start with all weapons, but for now only Telekinesis, Fire Magic, Ice Magic and Bolt Magic are in. Each spell has a Fire, Alt Fire, Zoom and Reload function
so make sure those keys are bound.

Telekinesis is your backup power
Main Fire focuses for a large push
Alt Fire does a weaker push 
Does not have a Zoom or Reload 

Fire Magic
Main Fire focuses flames on your target then explodes in a wide area
Alt Fire shoots a fireball
Reload summons three orbiters
Zoom heals you as long as its held down

Ice Magic
Main Fire focuses a storm on an area, stunning all inside before it ice bursts for large damage
Alt Fire does a ground based ice wave. It stunns whatever it hits and has a wide damage range
Reload summons an Ice Wall
Zoom invokes and equips Ice Armor

Electric Magic 
Main Fire focuses a bolt, stunning one monster before the lightning strikes for massive damage 
Alt Fire shoots a piercing lightning bolt that does set damage 
Reload shocks a small area around you and is fast to cast
Zoom stops time

Dark Magic
Main Fire focuses pure darkness, dealing full damage in a wide radius, fearing enemies and completely paralyzing the main target
Alt Fire shoots dark balls that deal light damage and fear
Reload turns you into a ghost
Zoom summons shadow clones

Arcane Magic
Main Fire evokes and throws 5 explosive magic orbs
Alt Fire shoots a small magic missile
Reload gives you DR for a time
Zoom drains health to refill Mana

Soul Magic
Main Fire deals damage all around you, and heals you for every enemy hit
Alt Fire does a instant kill melee attack
Reload summons a clone of yourself
Zoom revives monsters

Holy Magic - must be picked up. Takes health and mana to cast. Spells can hit yourself.
Main Fire focuses and explodes an area in Holy power
Alt Fire smites all foes in your vision to a range
Reload gives you and extra life
Zoom lays down a holy zone that protects and heals you